Retrieve IPython history:

- https://stackoverflow.com/questions/25124037/ipython-print-complete-history-not-just-current-session
- https://www.developpez.net/forums/d1173171/autres-langages/python/edi-rad/ipython-recuperation-l-historique-d-ancienne-session/

import readline                                                         

readline.read_history_file()                                            
readline.write_history_file()    
readline.read_history_file()                                       

%hist -o -g -f ipython_history.md                                       

________________________

 9/1: file = "/home/sakura/Test_Environment/horaires_travail.xlsx"
 9/2: import pandas as pd
 9/3: import pandas as pd
 9/4: file_name = "/home/sakura/Test_Environment/horaires_travail.xlsx"
 9/5: read_file = pd.read_excel(file_name)
 9/6: read_file = pd.read_excel(file_name)
 9/7: json_file = "/home/sakura/Test_Environment/horaires_travail.json"
 9/8: read_file.to_json(json_file, index = None, header = True)
 9/9: read_file.to_json(json_file)
9/10: import os
9/11: import os.path
9/12:
folder_path = input('enter file path (with the ending \"/\"):')
/home/sakura/Test_Environment
10/1: import os
10/2: import os.path
10/3: import pandas as pd
10/4: folder_path = input('enter file path (without the ending \"/\"):')
10/5: file_name = input('enter file name:')
10/6: json_file = input('enter new csv file name:')
10/7: full_path = folder_path + os.sep + file_name
10/8:
if os.path.isfile(full_path) and full_path.endswith('.xlsx'):
    read_file = pd.read_excel(full_path)
    read_file.to_json(folder_path + os.sep + json_file)
else:
    print("Something went wrong")
10/9: %save -r mysession 1-999999
11/1: import pathlib
11/2: pathlib.Path(__file__).parent.absolute()
11/3: pathlib.Path(__file__).parent.absolute()
11/4: pathlib.Path.absolute()
11/5: pathlib.Path().absolute()
11/6: import os
11/7: os.path.abspath(os.getcwd())
12/1: import os
12/2: os.path.abspath(os.getcwd())
12/3: import pathlib
12/4: pathlib.Path().absolute()
12/5: dir = os.path.abspath(os.getcwd())
12/6: print dir
12/7: print (dir)
12/8: for file in os.listdir(dir)
12/9: for file in os.listdir("dir")
12/10: directory = os.path.abspath(os.getcwd())
12/11: for file in os.listdir(directory)
12/12: for file in os.listdir("directory")
12/13: for file in os.listdir(directory)
12/14: print(directory)
12/15: for file in os.listdir( directory )
12/16: path = os.path.abspath(os.getcwd())
12/17: dirs = os.listdir(path)
12/18: print(dirs)
12/19: for file in dirs
12/20:
for file in dirs:
    if file.endswith(".ods")
12/21:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.join(dirs, file))
12/22:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.join(path, file))
12/23:
for file in dirs:
    if file.endswith(".ods"):
        sheet_idx = 1
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(file, index = None, header = True)
12/24: from pandas_ods_reader import read_ods
12/25:
for file in dirs:
    if file.endswith(".ods"):
        sheet_idx = 1
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(file, index = None, header = True)
12/26:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.splitext(path, file)[0])
12/27:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.splitext(file)[0])
12/28:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.splitext(file)[0].csv)
12/29:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.splitext(file)[0]) + .csv
12/30:
for file in dirs:
    if file.endswith(".ods"):
        print(os.path.splitext(file)[0]) + ".csv"
12/31:
for file in dirs:
    if file.endswith(".ods"):
        print((os.path.splitext(file)[0]).csv)
12/32:
for file in dirs:
    if file.endswith(".ods"):
        print((os.path.splitext(file)[0])".csv")
12/33:
for file in dirs:
    if file.endswith(".ods"):
        print((os.path.splitext(file)[0]) + .csv)
12/34:
for file in dirs:
    if file.endswith(".ods"):
        print((os.path.splitext(file)[0]), ".csv")
12/35:
for file in dirs:
    if file.endswith(".ods"):
        print((os.path.splitext(file)[0]), ".csv", sep='')
12/36:
for file in dirs:
    if file.endswith(".ods"):
        newname = print((os.path.splitext(file)[0]), ".csv", sep='')
12/37:
for file in dirs:
    if file.endswith(".ods"):
        newname = print((os.path.splitext(file)[0]), ".csv", sep='')
12/38: print(newname)
12/39:
for file in dirs:
    if file.endswith(".ods"):
        newname = print((os.path.splitext(file)[0]), ".csv", sep='')
        print(newname)
12/40: newname = os.path.splitext(file)[0] + ".csv"
12/41: print(newname)
   1:
import os
import os.path
from pandas_ods_reader import read_ods

path = os.path.abspath(os.getcwd())
dirs = os.listdir(path)


for file in dirs:
    newname = os.path.splitext(file)[0] + ".csv"
    sheet_idx = 1
    if file.endswith(".ods"):
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(newname, index = None, header = True)
else:
    print("Something went wrong")
   2:
import os
import os.path
from pandas_ods_reader import read_ods

path = os.path.abspath(os.getcwd())
dirs = os.listdir(path)


for file in dirs:
    newname = os.path.splitext(file)[0] + ".csv"
    sheet_idx = 1
    if file.endswith(".ods"):
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(newname, index = None, header = True)
   3:
import os
import os.path
from pandas_ods_reader import read_ods

path = os.path.abspath(os.getcwd())
dirs = os.listdir(path)


for file in dirs:
    newname = os.path.splitext(file)[0] + ".csv"
    sheet_idx = 1
    if file.endswith(".ods"):
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(newname, index = None, header = True)
   4:
import os
import os.path
from pandas_ods_reader import read_ods

path = os.path.abspath(os.getcwd())
dirs = os.listdir(path)


for file in dirs:
    newname = os.path.splitext(file)[0] + ".csv"
    sheet_idx = 1
    if file.endswith(".ods"):
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(newname, index = None, header = False)
   5: import readline
   6: readline.read_history_file()
   7: readline.write_history_file()
   8: %hist -o -g -f ipython_history.md
