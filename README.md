# Three scripts to convert .ods files to .csv, using Bash or Python

Three scripts to convert .ods files to .csv, using Bash or Python.

See also [my scripts to convert Excel files to JSON or CSV](https://gitlab.com/sakura-lain/python-scripts-convert-excel-csv-json/-/blob/master/README.md).