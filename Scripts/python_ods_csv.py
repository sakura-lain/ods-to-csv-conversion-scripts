#!/usr/bin/python3

import os
import os.path
from pandas_ods_reader import read_ods

folder_path = input('enter file path (without the ending \"/\"): ')
file_name = input('enter file name: ')
csv_file = input('enter new csv file name: ')

full_path = folder_path + os.sep + file_name
sheet_idx = 1

if os.path.isfile(full_path) and full_path.endswith('.ods'):
    read_file = read_ods(full_path, sheet_idx)
    read_file.to_csv(folder_path + os.sep + csv_file, index = None, header = True)
else:
    print("Something went wrong")
