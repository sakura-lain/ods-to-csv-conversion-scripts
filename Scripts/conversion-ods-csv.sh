#!/bin/bash

IFS=$'\n'

for i in $(find *.ods)
do
	unoconv -f csv -e FilterOptions="59,34,0,1" ${i}
done

for j in $(find *.csv)
do
	sed -i 's/"//g' ${j}
	sed -i 's/ //g' ${j}
	#iconv -f iso8859-1 -t utf8 ${j}
done
