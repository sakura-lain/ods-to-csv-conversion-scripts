#!/usr/bin/python3

import os
import os.path
from pandas_ods_reader import read_ods
import re

path = os.path.abspath(os.getcwd())
dirs = os.listdir(path)


for file in dirs:
    newname = os.path.splitext(file)[0] + ".csv"
    sheet_idx = 1
    if file.endswith(".ods"):
        read_file = read_ods(file, sheet_idx)
        read_file.to_csv(newname, sep=';', index = None, header = False)
